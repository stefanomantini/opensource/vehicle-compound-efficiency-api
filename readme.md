# Algorithms for efficient Vehicle Compound Management #

## Documentation Links ##
* Project Managed on [Trello](https://trello.com/b/ifoeiaq5/final-year-project)
* Report hosted on [Google Docs](https://docs.google.com/document/d/1SjvHETF4LALnisHmVhC8evwOdL4Cw8qF1CFW7IVMJEU/edit?usp=sharing)
* Supporting Documents hosted on [Google Drive](https://drive.google.com/folderview?id=0B4wDublaWjh0fnpyT1EtRjdTcC16UGhDNkNQMXdnNWlGZnJydEV5eTVZTE02cjdXNHNsRmM&usp=sharing)
## Development Links ##
* Code Hosted on [Bitbucket](https://bitbucket.org/sman6798/tsp)
* CI Hosted on [CodeShip](https://codeship.com/projects/122329)
* Site Hosted on [Heroku](https://vcm-tsp.herokuapp.com/#/)
* API Gateway on [Amazon API Gateway (AWS)](https://zfxqao5gij.execute-api.us-west-2.amazonaws.com/eu) *--Note: POST Requests Only*

Author: @sman6798
Contact: sman6798@gmail.com