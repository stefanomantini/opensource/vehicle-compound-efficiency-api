package com.stefanomantini.web.util;

import com.stefanomantini.solvers.objects.Tour;

/**
 * Wraps SolverRequestObject and a tour to show 
 * specified params of request in response
 * @author Stefano Mantini
 *
 */
public class SolverResponseObject {
	
	private String errorMessage;
	private SolverRequestObject request;
	private Tour shortestTour;

	public SolverResponseObject(){
		setErrorMessage(null);
		setRequest(null);
		setShortestTour(null);
	}

	public String getErrorMessage() {
		return errorMessage;
	}


	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}


	public SolverResponseObject(String errorMessage, SolverRequestObject request, Tour shortestTour){
		this.setErrorMessage(errorMessage);
		this.setRequest(request);
		this.setShortestTour(shortestTour);
	}



	/**
	 * @return the shortestTour
	 */
	public Tour getShortestTour() {
		return shortestTour;
	}


	/**
	 * @param shortestTour the shortestTour to set
	 */
	public void setShortestTour(Tour shortestTour) {
		this.shortestTour = shortestTour;
	}


	/**
	 * @return the request
	 */
	public SolverRequestObject getRequest() {
		return request;
	}


	/**
	 * @param request the request to set
	 */
	public void setRequest(SolverRequestObject request) {
		this.request = request;
	}
	
	
	@Override
	public String toString() {
		return "SolverResponseObject [errorMessage=" + errorMessage + ", request=" + request + ", shortestTour="
				+ shortestTour + "]";
	}



}
