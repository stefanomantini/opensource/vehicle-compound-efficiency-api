package com.stefanomantini.web.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.stefanomantini.config.Constants;
import com.stefanomantini.solvers.objects.Vehicle;
import com.stefanomantini.solvers.util.TestData;

/**
 * REST Controller for generating data
 * @author Stefano Mantini
 */
@Controller
@RestController
@RequestMapping("/api")
public class DataGenerationController {

	private static final Logger logger = LoggerFactory.getLogger(DataGenerationController.class);

	/**
	 * Generates nVehicle vehicles from a true random set
	 * @param nVehicles
	 * @param response
	 * @return
	 */
    @RequestMapping(value = "/generate-vehicles/random/n/{nVehicles}",
    		method = RequestMethod.GET,
    		produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<Vehicle>> getNVehicles(@PathVariable int nVehicles, HttpServletResponse response){
		logger.debug("Requesting " + nVehicles + " Random Vehicles");
		return new ResponseEntity<>(TestData.generateVehicles(nVehicles), HttpStatus.OK);
    }
    
    /**
     * Generates nVehicle vehicles from a seeded pseudorandom set
     * @param nVehicles
     * @param response
     * @return
     */
    @RequestMapping(value = "/generate-vehicles/random-seed/{seed}/{nVehicles}",
    		method = RequestMethod.GET,
    		produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ArrayList<Vehicle>> getNSampleVehicles(@PathVariable int nVehicles, @PathVariable String seed, HttpServletResponse response){
    	logger.debug("Solving for random set using Brute force for a given set of vehicles");
    	
    	return new ResponseEntity<>(TestData.generateSeededRandom(nVehicles, Long.parseLong(seed)), HttpStatus.OK);
    }

    /**
     * Generates nVehicle vehicles from a hardcoded set (MAX15)
     * @param nVehicles
     * @param response
     * @return
     */
    @RequestMapping(value = "/generate-vehicles/static-sample/{nVehicles}",
    		method = RequestMethod.GET,
    		produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<Vehicle>> getSampleVehicles(@PathVariable int nVehicles, HttpServletResponse response){
    	logger.debug("Requesting Sample set of Vehicles");
    	return new ResponseEntity<>(TestData.generateFromSample(nVehicles), HttpStatus.OK);
    }
    
    /**
     * Generates from a well-known TSP problem text file on the server (WARN: 100,000 nodes)
     * @param nVehicles
     * @param response
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/generate-vehicles/mona-sample/{nVehicles}",
    		method = RequestMethod.GET,
    		produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<Vehicle>> getMonaSample(@PathVariable int nVehicles, HttpServletResponse response) throws IOException{
		logger.debug("Requesting Sample set of Vehicles");
		return new ResponseEntity<>(TestData.getFromFile(Constants.DATA_MONA, nVehicles), HttpStatus.OK);
    }


}
