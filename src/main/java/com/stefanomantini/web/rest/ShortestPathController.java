package com.stefanomantini.web.rest;

import java.io.IOException;

import javax.naming.directory.InvalidAttributesException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.stefanomantini.web.util.Dispatcher;
import com.stefanomantini.web.util.SolverRequestObject;
import com.stefanomantini.web.util.SolverResponseObject;

/**
 * All solver paths prefixed with API
 * Exposes generic search POST API
 * @author Stefano
 */
@Controller
@RestController
@RequestMapping("/api")
public class ShortestPathController {
	
	private static final Logger logger = LoggerFactory.getLogger(ShortestPathController.class);
    
	/**
	 * Recieves request object for dispatch
	 * @param request
	 * @return
	 * @throws Exception
	 */
    @RequestMapping(value = "/solver/shortestPath",
    		method = RequestMethod.POST,
    		produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<SolverResponseObject> shortestPath(@RequestBody SolverRequestObject request) throws Exception{
    	
    	logger.debug("Solving using the following preset" + request);
    	SolverResponseObject response = new SolverResponseObject(null, request, Dispatcher.solverDispatch(request));
    	
    	if(response.getShortestTour().size() == 0){
    		response.setErrorMessage("Error: Invalid Parameters");
    		return new ResponseEntity<SolverResponseObject>(response, HttpStatus.BAD_REQUEST);
    	}
		
    	return new ResponseEntity<SolverResponseObject>(response, HttpStatus.OK);
    }
    
    /**
     * Displays message to POST to this URI
     */
    @RequestMapping(value="/solver/shortestPath",
    	method = RequestMethod.GET,
    	produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<SolverResponseObject> debugMessage(){
    	SolverResponseObject request = new SolverResponseObject("Method not allowed, POST requests only \n See documentation: " + "http://vcm-tsp.herokuapp.com/api-docs/index.html", null, null);
    	return new ResponseEntity<SolverResponseObject>(request, HttpStatus.BAD_REQUEST);
    }

    


}
