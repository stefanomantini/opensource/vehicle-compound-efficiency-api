package com.stefanomantini;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Application MAIN class for Spring Boot App
 * @author Stefano Mantini=
 */
@SpringBootApplication
public class Application {

	public static void main(String[] args) throws IOException {
        SpringApplication.run(Application.class, args);
	}
}