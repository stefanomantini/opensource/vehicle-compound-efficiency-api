package com.stefanomantini.solvers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Random;

import com.stefanomantini.solvers.objects.Tour;
import com.stefanomantini.solvers.objects.Vehicle;
import com.stefanomantini.solvers.util.EuclidianDistance;
import com.stefanomantini.solvers.util.TourComparator;
import com.stefanomantini.web.util.SolverRequestObject;

/**
 * Simulation of the way current business is performed
 * @author Stefano Mantini
 */
public class MorroccoTangiers {
	
	/**
	 * Uses a pre-set radius and chooses from the N nearest neighbours 
	 * according to the index to simulate line-of sight
	 * @param unvisited
	 * @param request
	 * @return
	 */
	public static Tour getNissanTour(ArrayList<Vehicle> unvisited, SolverRequestObject request) {
		ArrayList<Vehicle> visited = new ArrayList<Vehicle>();
		int radius = (int) Math.round(unvisited.size()/1.5);

		//find start node
		for (Iterator<Vehicle> iterator = unvisited.iterator(); iterator.hasNext();) {
		    Vehicle vehicle = iterator.next();
			if(vehicle.isStartNode()){
				visited.add(vehicle);
				iterator.remove();
				break;
			}
		}
		
		//while there are still unvisited nodes find nissanNearest
		while(unvisited.size() >= 1){
			Vehicle n = nissanMethod(visited.get(visited.size()-1), unvisited, radius, request);
			unvisited.remove(n);
			visited.add(n);
		}
		
		Tour nissanTour = new Tour(visited, EuclidianDistance.calcCollectionFromRequest(visited, request));		
		return nissanTour;
	}

	/**
	 * Calculates ED between next and every other node
	 * orders the list by ED, and chooses randomly from 
	 * the shortest 3
	 * randomly from the 3 closest
	 * @param next
	 * @param unvisited
	 * @return
	 */
	private static Vehicle nissanMethod(Vehicle next, ArrayList<Vehicle> unvisited, int radius, SolverRequestObject request) {
		ArrayList<Tour> tempNearest = new ArrayList<Tour>();
		
		for(Vehicle uVehicle: unvisited){
			double dist = EuclidianDistance.calcPairFromRequest(next, uVehicle, request);
			ArrayList<Vehicle> temp = new ArrayList<Vehicle>();
			temp.add(uVehicle);
			Tour segment = new Tour(temp, dist);
			tempNearest.add(segment);

		}
		//sort shortest tour first
		//cannot deal with lots
		Collections.sort(tempNearest, new TourComparator());
		
		
		//random between 0 and radius
		int r = random_int(0, radius);
		if(r >= unvisited.size()){
			r = 0;
		}
		return tempNearest.get(r).getVehicles().get(0);
	}
	
	/**
	 * helper method to generate a random int on request
	 * @param Min
	 * @param Max
	 * @return
	 */
	public static int random_int(int Min, int Max)
	{
	     return (int) (Math.random()*(Max-Min))+Min;
	}


}
