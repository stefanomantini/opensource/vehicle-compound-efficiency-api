package com.stefanomantini.solvers.objects;

import com.stefanomantini.solvers.util.Coord;

/**
 * Standard Vehicle Class
 * @author Stefano Mantini
 *
 */
public class Vehicle {

	private String invariant;
	private boolean startNode;
	private String businessType;
	private Coord<?, ?> currentParkingPosition;
	private Coord<?, ?> nextParkingPosition;

	public Vehicle(String invariant, boolean startNode, String businessType, Coord<?, ?> currentParkingPosition, Coord<?, ?> nextParkingPosition){
		this.invariant = invariant;
		this.startNode = startNode;	
		this.businessType = businessType;
		this.setCurrentParkingPosition(currentParkingPosition);
		this.setNextParkingPosition(nextParkingPosition);
	}
	
	/**
	 * @return the invariant
	 */
	public String getInvariant() {
		return invariant;
	}
	/**
	 * @param invariant the invariant to set
	 */
	public void setInvariant(String invariant) {
		this.invariant = invariant;
	}


	
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Vehicle vehicle = (Vehicle) o;

        if (!vehicle.equals(vehicle.invariant)) {
            return false;
        }

        return true;
    }

	/**
	 * @return the startNodeFlag
	 */
	public boolean isStartNode() {
		return startNode;
	}

	/**
	 * 
	 * @param startNode
	 */
	public void setStartNode(boolean startNode) {
		this.startNode = startNode;
	}

	/**
	 * @return the currentParkingPosition
	 */
	public Coord<?, ?> getCurrentParkingPosition() {
		return currentParkingPosition;
	}

	/**
	 * @param currentParkingPosition the currentParkingPosition to set
	 */
	public void setCurrentParkingPosition(Coord<?, ?> currentParkingPosition) {
		this.currentParkingPosition = currentParkingPosition;
	}

	/**
	 * @return the nextParkingPosition
	 */
	public Coord<?, ?> getNextParkingPosition() {
		return nextParkingPosition;
	}

	/**
	 * @param nextParkingPosition the nextParkingPosition to set
	 */
	public void setNextParkingPosition(Coord<?, ?> nextParkingPosition) {
		this.nextParkingPosition = nextParkingPosition;
	}
	

	@Override
	public String toString() {
		return "Vehicle [invariant=" + invariant + ", startNode=" + startNode + ", businessType=" + businessType
				+ ", currentParkingPosition=" + currentParkingPosition + ", nextParkingPosition=" + nextParkingPosition
				+ "]";
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}
}
