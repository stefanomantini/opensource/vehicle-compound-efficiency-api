package com.stefanomantini.solvers.objects;

import java.util.ArrayList;
import java.util.List;

import com.stefanomantini.solvers.util.EuclidianDistance;

/**
 * Holds a tour and an associated weight
 * @author Stefano Mantini
 *
 */
public class Tour {
	
	private double totalEuclidianDistance;
	private ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();

	
	public Tour(){
		totalEuclidianDistance = 0;
		vehicles = new ArrayList<Vehicle>();
	}
	
	public Tour(List<Vehicle> perm, double totalEuclideanDistance){
		this.setTotalEuclidianDistance(totalEuclideanDistance);
		this.setVehicles((ArrayList<Vehicle>) perm);
	}
	
	public Tour populateTour(ArrayList<Vehicle> vehicles){
		Tour tour = new Tour(vehicles, EuclidianDistance.fullEDCalcforCollection(vehicles));
		return tour;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Tour [vehicles=" + vehicles + ", totalEuclidianDistance=" + totalEuclidianDistance + "]";
	}

	/**
	 * @return the vehicles
	 */
	public ArrayList<Vehicle> getVehicles() {
		return vehicles;
	}

	/**
	 * @param vehicles the vehicles to set
	 */
	public void setVehicles(ArrayList<Vehicle> vehicles) {
		this.vehicles = vehicles;
	}

	/**
	 * @return the totalEuclidianDistance
	 */
	public double getTotalEuclidianDistance() {
		return totalEuclidianDistance;
	}

	/**
	 * @param totalEuclidianDistance the totalEuclidianDistance to set
	 */
	public void setTotalEuclidianDistance(double totalEuclidianDistance) {
		this.totalEuclidianDistance = totalEuclidianDistance;
	}

	public int size() {
		return getVehicles().size();
	}

}
