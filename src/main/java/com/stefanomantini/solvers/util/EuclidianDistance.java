package com.stefanomantini.solvers.util;

import java.util.ArrayList;

import com.stefanomantini.solvers.objects.Vehicle;
import com.stefanomantini.web.util.SolverRequestObject;

public class EuclidianDistance {

	/**
	 * Helper function dispatches to other ED calc collection methods based on request
	 * @param tour
	 * @param request
	 * @return
	 */
	public static Double calcCollectionFromRequest(ArrayList<Vehicle> tour, SolverRequestObject request) {
		double totalED = 0;
		if(request.getCalc().equals("ed1122")){
			for (int i = 0; i < tour.size() - 1; i++) {
				totalED = totalED + eDistanceCalcPair(tour.get(i), tour.get(i + 1));
			}
		}else if(request.getCalc().equals("ed12")){
			for (int i = 0; i < tour.size() - 1; i++) {
				totalED = totalED + eDistanceCalcPairStartOnly(tour.get(i), tour.get(i + 1));
			}
		}else if(request.getCalc().equals("wed1122")){
			for (int i = 0; i < tour.size() - 1; i++) {
				totalED = totalED + weightedEDCalc(tour.get(i), tour.get(i + 1), request.getWalkWeight());
			}
		}
		return totalED;
	}
	
	/**
	 * Helper function dispatches to other ED pair methods based on request
	 * @param a
	 * @param b
	 * @param request
	 * @return
	 */
	public static double calcPairFromRequest(Vehicle a, Vehicle b, SolverRequestObject request) {
		
		double totalEuclidianDistance = 0;
		
		if(request.getCalc().equals("ed1122")){
			totalEuclidianDistance = eDistanceCalcPair(a, b);
		}
		else if(request.getCalc().equals("ed12")){
			totalEuclidianDistance = eDistanceCalcPairStartOnly(a, b);
		}
		else if(request.getCalc().equals("wed1122")){
			totalEuclidianDistance = weightedEDCalc(a, b, request.getWalkWeight());
		}

		return totalEuclidianDistance;
	}
	
	
	

	/**
	 * Full ED calculation collection
	 * @param tour
	 * @return
	 */
	public static Double fullEDCalcforCollection(ArrayList<Vehicle> tour) {
		double totalED = 0;
		for (int i = 0; i < tour.size() - 1; i++) {
			totalED = totalED + eDistanceCalcPair(tour.get(i), tour.get(i + 1));
		}
		return totalED;
	}

	/**
	 * Part ED calc for collection
	 * @param tour
	 * @return
	 */
	public static Double partEDCalcforCollection(ArrayList<Vehicle> tour) {
		double totalED = 0;
		for (int i = 0; i < tour.size() - 1; i++) {
			totalED = totalED + eDistanceCalcPairStartOnly(tour.get(i), tour.get(i + 1));
		}
		return totalED;
	}
	
	/**
	 * Weighted ED calc for collection
	 * @param tour
	 * @param response
	 * @return
	 */
	public static Double weightedEDCalcforCollection(ArrayList<Vehicle> tour, SolverRequestObject response) {
		double totalED = 0;
		for (int i = 0; i < tour.size() - 1; i++) {
			totalED = totalED + weightedEDCalc(tour.get(i), tour.get(i + 1), response.getWalkWeight());
		}
		return totalED;
	}


	/**
	 * Gets full ED for a single edge between two vehicles 
	 * taking into account pickup and dropoff and treating 
	 * it as a single edge
	 * @param a
	 * @param b
	 * @return
	 */
	public static double eDistanceCalcPair(Vehicle a, Vehicle b) {

		double aStartX = a.getCurrentParkingPosition().getX();
		double aStartY = a.getCurrentParkingPosition().getY();
		double aEndX = a.getNextParkingPosition().getX();
		double aEndY = a.getNextParkingPosition().getY();

		double bStartX = b.getCurrentParkingPosition().getY();
		double bStartY = b.getCurrentParkingPosition().getY();
		double bEndX = b.getNextParkingPosition().getY();
		double bEndY = b.getNextParkingPosition().getY();

		double distAX = aStartX - aEndX;
		double distAY = aStartY - aEndY;

		double distBY = bStartX - bEndX;
		double distBX = bStartY - bEndY;

		double distABX = aEndX - bStartX;
		double distABY = aEndY - bStartY;

		double euclidianDistanceA = Math.sqrt(distAX * distAX + distAY * distAY);
		double euclideanDistanceB = Math.sqrt(distBX * distBX + distBY * distBY);
		double euclidianDistanceI = Math.sqrt(distABX * distABX + distABY * distABY);
		double totalEuclidianDistance = euclidianDistanceA + euclideanDistanceB + euclidianDistanceI;

		return totalEuclidianDistance;
	}

	/**
	 * Naive pickup only ED calculation 
	 * @deprecated
	 * @param a
	 * @param b
	 * @return
	 */
	public static double eDistanceCalcPairStartOnly(Vehicle a, Vehicle b) {

		double aStartX = a.getCurrentParkingPosition().getX();
		double aStartY = a.getCurrentParkingPosition().getY();

		double bStartX = b.getCurrentParkingPosition().getY();
		double bStartY = b.getCurrentParkingPosition().getY();

		double distABX = aStartX - bStartX;
		double distABY = aStartY - bStartY;

		double euclidianDistance = Math.sqrt(distABX * distABX + distABY * distABY);

		return euclidianDistance;
	}



	/**
	 * Weighted ED calc for a pair of vehicles
	 * @param a
	 * @param b
	 * @param walkWeight
	 * @return
	 */
	public static double weightedEDCalc(Vehicle a, Vehicle b, int walkWeight) {
		if(walkWeight == 0){
			walkWeight = 5;
		}
		
		double aStartX = a.getCurrentParkingPosition().getX();
		double aStartY = a.getCurrentParkingPosition().getY();
		double aEndX = a.getNextParkingPosition().getX();
		double aEndY = a.getNextParkingPosition().getY();

		double bStartX = b.getCurrentParkingPosition().getY();
		double bStartY = b.getCurrentParkingPosition().getY();
		double bEndX = b.getNextParkingPosition().getY();
		double bEndY = b.getNextParkingPosition().getY();

		double distAX = aStartX - aEndX;
		double distAY = aStartY - aEndY;

		double distBY = bStartX - bEndX;
		double distBX = bStartY - bEndY;

		double distABX = aEndX - bStartX;
		double distABY = aEndY - bStartY;

		double euclidianDistanceA = Math.sqrt(distAX * distAX + distAY * distAY);
		double euclideanDistanceB = Math.sqrt(distBX * distBX + distBY * distBY);
		double euclidianDistanceI = Math.sqrt(distABX * distABX + distABY * distABY);
		double totalEuclidianDistance = euclidianDistanceA + euclideanDistanceB + (euclidianDistanceI*walkWeight);

		return totalEuclidianDistance;
	}
	
	/**
	 * RAW calculation using just doubles for coordinates
	 * @deprecated not currently used
	 * @param a
	 * @param b
	 * @param x
	 * @param y
	 * @return
	 */
	public static double eDistanceCalcDouble(double a, double b, double x, double y) {
		double euclidianDistance;

		double aX = a;
		double aY = b;

		double bX = x;
		double bY = y;

		double dX = aX - bX;
		double dY = aY - bY;

		euclidianDistance = Math.sqrt(dX * dX + dY * dY);

		return euclidianDistance;
	}

}
