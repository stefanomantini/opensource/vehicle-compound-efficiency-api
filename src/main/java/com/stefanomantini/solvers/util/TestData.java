package com.stefanomantini.solvers.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ResourceUtils;

import com.stefanomantini.solvers.objects.Vehicle;
import com.stefanomantini.web.util.SolverRequestObject;

/**
 * Various methods of generating data for Testing
 * @author Stefano
 */
public class TestData{

	private static final Logger logger = LoggerFactory.getLogger(TestData.class);
	private static Scanner scnr;

	/**
	 * Generates Vehicle Objects with true random
	 * Invariants and Parking Coordinates
	 * @param nVehicles
	 * @return
	 */
	public static ArrayList<Vehicle> generateVehicles(int nVehicles) {

		ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();

		/* Create a set of vehicles  */
		Coord<?, ?> baseNode = new Coord<Object, Object>(0,0);
		Vehicle startNode = new Vehicle("STARTNODE", true, "rn", baseNode, baseNode);
		vehicles.add(startNode);

		for(int i = 0; i<nVehicles-1; i++){
			Coord<?, ?> start = new Coord<Object, Object>(Math.round(Math.random()*100), Math.round(Math.random()*100));
			Coord<?, ?> end = new Coord<Object, Object>(Math.round(Math.random()*100), Math.round(Math.random()*100));
			Vehicle vehicle = new Vehicle(	RandomGenerator.getSaltString(),
										false,
										"rn", start,
										end
										);
			vehicles.add(vehicle);
		}
    	logger.debug("Generated nVehicles: " + nVehicles + " as the following Set: " + vehicles);
		return vehicles;

	}
	
	public static ArrayList<Vehicle> getFromNissanTxt() throws IOException {
		File file = ResourceUtils.getFile("CLE_TANGIER_RMA.txt");
        
        scnr = new Scanner(file);
      
        ArrayList<String> fileAsArray = new ArrayList<String>();
        ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();
        
        //Reading each line of file using Scanner class
        while(scnr.hasNextLine()){
            String line = scnr.nextLine();
            fileAsArray.add(line);
        }

		for(int i = 0; i < fileAsArray.size()-1; i++) {
			
    		String[] part1 = fileAsArray.get(i).split(" ");
    		String id1 = part1[0];
    		    					
			Vehicle v = new Vehicle(id1, false, "rn", null, null);
			vehicles.add(v);
    		i++;
		}		
		return vehicles;
        
	}
	
	/**
	 * parses a preset file format for TSP tours and converts to 
	 * objects with pickup and dropoff points file[1]+file[2]= Vehicle 1
	 * @param fileName
	 * @param nVehicles 
	 * @return
	 * @throws IOException
	 */
	public static ArrayList<Vehicle> getFromFile(String fileName, int nVehicles) throws IOException {
		File file = ResourceUtils.getFile(fileName);
        
        scnr = new Scanner(file);
      
        ArrayList<String> fileAsArray = new ArrayList<String>();
        ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();
        
        int max=0;
        //Reading each line of file using Scanner class
        while(scnr.hasNextLine() && max < nVehicles){
            String line = scnr.nextLine();
            fileAsArray.add(line);
            max++;
        }
        
        Coord<?,?> pickup = null;
        Coord<?,?> dropoff = null;
        
		for(int i = 0; i < fileAsArray.size()-1; i++) {
    		String[] part1 = fileAsArray.get(i).split(" ");
    		String id1 = part1[0];
    		Double x1 = Double.parseDouble(part1[1]); 
    		Double y1 = Double.parseDouble(part1[2]);  
    		
    		String[] part2 = fileAsArray.get(i+1).split(" ");
    		Double x2 = Double.parseDouble(part2[1]); 
    		Double y2 = Double.parseDouble(part2[2]);  
    		
			pickup = new Coord<Object,Object>(x1, y1);
			dropoff = new Coord<Object,Object>(x2, y2);
			
			Vehicle v = new Vehicle(id1, false, "rn", pickup, dropoff);
			vehicles.add(v);
    		i++;
		}
        
		return vehicles;
	}
	
	/**
	 * Generates given set of Vehicle Objects with
	 * preset invariants and Parking Coordinates
	 * @param nSample
	 * @return Arraylist of Vehicles
	 */
	public static <E> ArrayList<Vehicle> generateFromSample(int nSample) {
				
		ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();
		
    		Coord<?, ?> onecurrentParkingPosition = new Coord<Object, Object>(0,0);
    		Coord<?, ?> onenextParkingPosition = new Coord<Object, Object>(0,0);
    		Vehicle one = 		new Vehicle("STARTNODE", true, "rn", onecurrentParkingPosition, onenextParkingPosition);

    		Coord<?, ?> twocurrentParkingPosition = new Coord<Object, Object>(251,859);
    		Coord<?, ?> twoonenextParkingPosition = new Coord<Object, Object>(51, 959);
    		Vehicle two = 		new Vehicle("SGH001002002", false, "rn", twocurrentParkingPosition, twoonenextParkingPosition);
    		
    		Coord<?, ?> threeonecurrentParkingPosition = new Coord<Object, Object>(32, 532);
    		Coord<?, ?> threenextParkingPosition = new Coord<Object, Object>(159,456);
    		Vehicle three = 	new Vehicle("SGH001002003", false, "rn", threeonecurrentParkingPosition, threenextParkingPosition);
    		
    		Coord<?, ?> fourcurrentParkingPosition = new Coord<Object, Object>(866,835);
    		Coord<?, ?> fournextParkingPosition = new Coord<Object, Object>(711,44);
    		Vehicle four = 		new Vehicle("SGH001002004", false, "rn", fourcurrentParkingPosition, fournextParkingPosition);
    		
    		Coord<?, ?> fivecurrentParkingPosition = new Coord<Object, Object>(411,810);
    		Coord<?, ?> fivenextParkingPosition = new Coord<Object, Object>(743,385);
    		Vehicle five = 		new Vehicle("SGH001002005", false, "rn", fivecurrentParkingPosition, fivenextParkingPosition);
    		
    		Coord<?, ?> sixcurrentParkingPosition = new Coord<Object, Object>(272,968);
    		Coord<?, ?> sixnextParkingPosition = new Coord<Object, Object>(167,462);
    		Vehicle six = 		new Vehicle("SGH001002006", false, "rn", sixcurrentParkingPosition, sixnextParkingPosition);
    		
    		Coord<?, ?> sevencurrentParkingPosition = new Coord<Object, Object>(157,357);
    		Coord<?, ?> sevennextParkingPosition = new Coord<Object, Object>(566,251);
    		Vehicle seven = 	new Vehicle("SGH001002007", false, "rn", sevencurrentParkingPosition, sevennextParkingPosition);
    		
    		Coord<?, ?> eightcurrentParkingPosition = new Coord<Object, Object>(121,320);
    		Coord<?, ?> eightnextParkingPosition = new Coord<Object, Object>(33,444);
    		Vehicle eight = 	new Vehicle("SGH001002008", false, "rn", eightcurrentParkingPosition, eightnextParkingPosition);
    		
    		Coord<?, ?> ninecurrentParkingPosition = new Coord<Object, Object>(564,756);
    		Coord<?, ?> ninenextParkingPosition = new Coord<Object, Object>(88,384);
			Vehicle nine = 		new Vehicle("SGH001002009", false, "rn", ninecurrentParkingPosition, ninenextParkingPosition);
			
    		Coord<?, ?> tencurrentParkingPosition = new Coord<Object, Object>(142,764);
    		Coord<?, ?> tennextParkingPosition = new Coord<Object, Object>(421,633);
			Vehicle ten = 		new Vehicle("SGH001002010", false, "rn", tencurrentParkingPosition, tennextParkingPosition);
		
			
			Coord<?, ?> elevencurrentParkingPosition = new Coord<Object, Object>(645,144);
			Coord<?, ?> elevennextParkingPosition = new Coord<Object, Object>(144, 877);
			Vehicle eleven = 	new Vehicle("SGH001002011", false, "rn", elevencurrentParkingPosition, elevennextParkingPosition);
			
			Coord<?, ?> twelvecurrentParkingPosition = new Coord<Object, Object>(161,750);
			Coord<?, ?> twelvenextParkingPosition = new Coord<Object, Object>(53,644);
			Vehicle twelve = 	new Vehicle("SGH001002012", false, "rn", twelvecurrentParkingPosition, twelvenextParkingPosition);
			
			Coord<?, ?> thirteencurrentParkingPosition = new Coord<Object, Object>(506,608);
			Coord<?, ?> thirteennextParkingPosition = new Coord<Object, Object>(21,325);
			Vehicle thirteen = 	new Vehicle("SGH001002013", false, "rn", thirteencurrentParkingPosition, thirteennextParkingPosition);
			
			Coord<?, ?> fourteencurrentParkingPosition = new Coord<Object, Object>(123,81);
			Coord<?, ?> fourteennextParkingPosition = new Coord<Object, Object>(325,399);
			Vehicle fourteen = 	new Vehicle("SGH001002014", false, "rn", fourteencurrentParkingPosition, fourteennextParkingPosition);
			
			Coord<?, ?> fifteencurrentParkingPosition = new Coord<Object, Object>(100,891);
			Coord<?, ?> fifteennextParkingPosition = new Coord<Object, Object>(999,991);
			Vehicle fifteen = 	new Vehicle("SGH001002015", false, "rn", fifteencurrentParkingPosition, fifteennextParkingPosition);
			
			//add to list
			vehicles.add(one);
			vehicles.add(two);
			vehicles.add(three);
			vehicles.add(four);
			vehicles.add(five);
			vehicles.add(six);
			vehicles.add(seven);
			vehicles.add(eight);
			vehicles.add(nine);
			vehicles.add(ten);
			vehicles.add(eleven);
			vehicles.add(twelve);
			vehicles.add(thirteen);
			vehicles.add(fourteen);
			vehicles.add(fifteen);
			
			
		if(nSample > vehicles.size()){
			return null;
		}
		
		ArrayList<Vehicle> numVehicles = new ArrayList<Vehicle>();
		for(int i=0;i<nSample;i++){
			numVehicles.add(vehicles.get(i));
		}

		logger.debug("Set generated using provided sample set: " + numVehicles);
		return numVehicles;

	}

	
	/**
	 * Uses a seed to generate a pseudorandom tour
	 * @param nVehicles
	 * @return
	 */
	public static ArrayList<Vehicle> generateSeededRandom(int nVehicles, long seedVal) {
    	//set seed	
		
		//benchmark performed with 149L
    	Random seed = new Random(seedVal);
    	
    	ArrayList<Vehicle> sample = new ArrayList<Vehicle>();
    	
		Coord<?, ?> baseNode = new Coord<Object, Object>(0,0);
		Vehicle startNode = new Vehicle("STARTNODE", true, "rn", baseNode, baseNode);
		sample.add(startNode);
    	
    	
    	for ( int i=0; i<nVehicles-1; i++ ){
     	   double ax = seed.nextInt(10000);
    	   double ay = seed.nextInt(10000);
     	   double bx = seed.nextInt(10000);
    	   double by = seed.nextInt(10000);    	   
    	   Coord<?, ?> pickup = new Coord<>(ax, ay);
    	   Coord<?, ?> dropoff = new Coord<>(bx, by);
    	   String id = (i+1)+":"+RandomGenerator.getSaltString();
    	   Vehicle v = new Vehicle(id, false, "rn", pickup, dropoff);
    	   sample.add(v);
    	}
    	    	
		return sample;
	}
}
