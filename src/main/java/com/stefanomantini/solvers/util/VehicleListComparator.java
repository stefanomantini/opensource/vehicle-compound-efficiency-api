package com.stefanomantini.solvers.util;

import java.util.ArrayList;
import java.util.Comparator;

import com.stefanomantini.solvers.objects.Vehicle;

public class VehicleListComparator implements Comparator<ArrayList<Vehicle>> {
	/**
	 * Compares two lists of vehicles  
	 * calculated ED on-the-fly
	 * @param o1
	 * @param o2
	 * @return shorter edge
	 */
    @Override
    public int compare(ArrayList<Vehicle> o1, ArrayList<Vehicle> o2) {
        return (int) ( EuclidianDistance.fullEDCalcforCollection(o1) - 
        				EuclidianDistance.fullEDCalcforCollection(o2)  );
    }

}
