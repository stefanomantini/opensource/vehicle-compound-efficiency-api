package com.stefanomantini.solvers.util;

import java.util.ArrayList;
import java.util.List;

import com.stefanomantini.solvers.objects.Vehicle;

public class Duplicates {

	/**
	 * Takes a list and checks for duplicates 
	 * @param vehicles
	 * @return boolean 
	 */
	public static boolean hasDuplicates(List<Vehicle> vehicles) {
		//TODO move to util package & genericise
	    final List<String> usedNames = new ArrayList<String>();
	    for (Vehicle vehicle : vehicles) {
	        final String name = vehicle.getInvariant();
	        if (usedNames.contains(name)) {
	            return true;
	        }
	        usedNames.add(name);
	    }
	    return false;
	}

}
