package com.stefanomantini.solvers.util;

import java.util.Random;

public class RandomGenerator {
	
	/**
	 * Returns a pseudorandom alphanumeric for use as VIN
	 * @return
	 */
	public static String getSaltString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuffer salt = new StringBuffer(); //thread safe
        Random rnd = new Random(249L);
        while (salt.length() < 18) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }
	
}
