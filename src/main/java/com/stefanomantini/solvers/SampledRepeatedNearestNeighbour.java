package com.stefanomantini.solvers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import com.stefanomantini.solvers.objects.Tour;
import com.stefanomantini.solvers.objects.Vehicle;
import com.stefanomantini.web.util.SolverRequestObject;


/**
 * Takes a sample from the shuffled tour and tries nearest neighbour
 * with the different startpoints (not including startNode)
 * @author Stefano Mantini
 *
 */
public class SampledRepeatedNearestNeighbour {

	/**
	 * Sampled Repeated Nearest Neighbor Algorithm: use ~1/3 of the cities
	 * as starting point and choose the shortest from the resulting tours
	 * @param unvisited
	 * @param request 
	 * @return
	 */
	public static Tour calculateSampledRepeatedNearestNeighbour(ArrayList<Vehicle> unvisited, SolverRequestObject request) {
		ArrayList<Vehicle> visited = new ArrayList<Vehicle>();
		Tour shortestTour = null;
		double min = Double.MAX_VALUE;
		Vehicle start = null;
		
		//sample is 1/3 of unvisited
		int srnnSample = Math.round(unvisited.size()/3);
		if(srnnSample <= 1 ){srnnSample = 2;}
		
		
		ArrayList<Vehicle> sampledVeh = new ArrayList<Vehicle>();

		// find start node
		for (Iterator<Vehicle> iterator = unvisited.iterator(); iterator.hasNext();) {
			Vehicle vehicle = iterator.next();
			// iterator.remove();
			if (vehicle.isStartNode()) {
				visited.add(vehicle);
				sampledVeh.addAll(unvisited);
				start = vehicle;
				iterator.remove();
				break;
			}
		}
		
		//randomise unvisited
		Collections.shuffle(unvisited);
		
		//construct partial tours for the sample size from remaining
		ArrayList<ArrayList<Vehicle>> repeats = new ArrayList<ArrayList<Vehicle>>();
		for(int i = 0; i < srnnSample; i++){
			ArrayList<Vehicle> temp = new ArrayList<Vehicle>();
			temp.add(start);
			temp.add(unvisited.get(i));
			repeats.add(temp);
		}
		
			//sends all repetition stubs to get resolved and checks shortest
			for(ArrayList<Vehicle> vis : repeats){
				//create unvisited from visited and initial
				ArrayList<Vehicle> unvis = new ArrayList<Vehicle>(unvisited);
				unvis.removeAll(vis);		
				
				//find NN for this iteration
				Tour thisIteration = RepeatedNearestNeighbour.nearestNeighbourHelper(vis, unvis, request);
				
				//check for min
				if(min>thisIteration.getTotalEuclidianDistance()){
					min = thisIteration.getTotalEuclidianDistance();
					shortestTour = thisIteration;
				}
			}
			
		return shortestTour;
	}

}
