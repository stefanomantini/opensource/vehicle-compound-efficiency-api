package com.stefanomantini.solvers;

import java.util.ArrayList;
import java.util.List;

import com.stefanomantini.solvers.objects.Tour;
import com.stefanomantini.solvers.objects.Vehicle;
import com.stefanomantini.solvers.util.EuclidianDistance;
import com.stefanomantini.web.util.SolverRequestObject;

public class Unsorted {
	public static Tour calc(List<Vehicle> l, SolverRequestObject request) {
		
		double dist = EuclidianDistance.calcCollectionFromRequest((ArrayList<Vehicle>) l, request);
		Tour t = new Tour(l, dist);
		
		return t;
	}

}
