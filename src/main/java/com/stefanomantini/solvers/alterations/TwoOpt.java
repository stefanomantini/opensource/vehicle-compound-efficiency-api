package com.stefanomantini.solvers.alterations;

import java.util.ArrayList;
import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.stefanomantini.solvers.objects.Tour;
import com.stefanomantini.solvers.objects.Vehicle;
import com.stefanomantini.solvers.util.EuclidianDistance;
import com.stefanomantini.web.util.SolverRequestObject;

/**
 * Take a route that crosses itself and reorder it so that it does not
 * @author Stefano Mantini
 *
 */
public class TwoOpt{

	
	private static final Logger logger = LoggerFactory.getLogger(TwoOpt.class);

	/**
	 * Performs the 2-opt swap optimisation
	 * X A B C D Y  --start
	 * X A C B D Y --after opt
	 * the startNode isn't accounted for in the actual calculation so is therefore left out of the optimisation
	 * @param locations
	 * @return
	 */
	public static Tour execute(Tour locations, SolverRequestObject request) {
		logger.info("Begin 2-opt optimisation of tour");
		
		ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>(locations.getVehicles());
		
		for(Vehicle v : vehicles){
			double initialDist = EuclidianDistance.calcCollectionFromRequest(vehicles, request);

	        for (int i = 1; i < vehicles.size()-3; i++) {
                Vehicle a = vehicles.get(i);
                Vehicle b = vehicles.get(i+1);
                Vehicle c = vehicles.get(i+2);
                Vehicle d = vehicles.get(i+3);
                
                double distanceab = EuclidianDistance.calcPairFromRequest(a, b, request);
                double distancebd = EuclidianDistance.calcPairFromRequest(b, d, request);
                
                double distanceac = EuclidianDistance.calcPairFromRequest(a, c, request);
                double distancecd = EuclidianDistance.calcPairFromRequest(c, d, request);

                double abcd = distanceab+distancecd;
                double acbd = distanceac+distancebd;
                
                if(acbd<abcd){
                	double initialDist1 = EuclidianDistance.calcCollectionFromRequest(vehicles, request);
                	Collections.swap(vehicles, i+1, i+2);
                	double nextDist = EuclidianDistance.calcCollectionFromRequest(vehicles, request);
        			logger.debug("Swapped:" + vehicles.get(i+1) +" with: "+ vehicles.get(i+2));
                	//swap back if affects the overall length of tour due to dropoff point
                	if(nextDist > initialDist1){
                    	Collections.swap(vehicles, i+2, i+1);
            			logger.debug("Reverted swap due to dropoff point for vehicle :" + vehicles.get(i+2) +" with: "+ vehicles.get(i+1));
                	}
                }
            }
	        double newDist = EuclidianDistance.calcCollectionFromRequest(vehicles, request);
	        
	        //if no optimisation is made on a whole tour, exit loop
	        if(newDist == initialDist){
	        	locations.setVehicles(vehicles);		
	        	locations.setTotalEuclidianDistance(EuclidianDistance.calcCollectionFromRequest(vehicles, request));
	        	break;
	        }
        }
		return locations;

	}

}

