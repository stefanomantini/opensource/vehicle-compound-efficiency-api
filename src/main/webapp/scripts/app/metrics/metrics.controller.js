'use strict';

angular.module('vcm-tsp')
    .controller('MetricsController', function ($scope, MonitoringService, $uibModal, $http, CodeShipAPI) {
        $scope.metrics = {};
        $scope.updatingMetrics = true;

        $scope.refresh = function () {
            $scope.updatingMetrics = true;
            MonitoringService.getMetrics().then(function (promise) {
                $scope.metrics = promise;
                $scope.updatingMetrics = false;
            }, function (promise) {
                $scope.metrics = promise.data;
                $scope.updatingMetrics = false;
            });
            console.log($scope.metrics);
            console.log($scope.servicesStats)
        };

        $scope.$watch('metrics', function (newValue) {
            $scope.servicesStats = {};
            $scope.cachesStats = {};
            angular.forEach(newValue.timers, function (value, key) {
            	console.log(value + key);
                if (key.indexOf('web.rest') !== -1 || key.indexOf('service') !== -1) {
                    $scope.servicesStats[key] = value;
                }
                if (key.indexOf('net.sf.ehcache.Cache') !== -1) {
                    // remove gets or puts
                    var index = key.lastIndexOf('.');
                    var newKey = key.substr(0, index);

                    // Keep the name of the domain
                    index = newKey.lastIndexOf('.');
                    $scope.cachesStats[newKey] = {
                        'name': newKey.substr(index + 1),
                        'value': value
                    };
                }
            });
        });

        $scope.refresh();

        $scope.refreshThreadDumpData = function() {
            MonitoringService.threadDump().then(function(data) {

                var modalInstance = $uibModal.open({
                    templateUrl: 'scripts/app/metrics/metrics.modal.html',
                    controller: 'MetricsModalController',
                    size: 'lg',
                    resolve: {
                        threadDump: function() {
                            return data.content;
                        }

                    }
                });
            });
        };
        
        
    	$scope.response = {};
    	$scope.response.codeship = {};
    	$scope.response.trace = {};
    	
    	$http({
    		method: 'GET',
    		url: '/metrics/trace'
    	}).then(function successCallback(response) {
    		$scope.response.trace = response;
    		console.log($scope.response);
    	}, function errorCallback(response) {
    		alert("Trace request failed");
    	});
    	
    	$http({
    		  method: 'GET',
    		  url: CodeShipAPI.uri()
    		}).then(function successCallback(response) {
    			$scope.response.codeship = response;
    		  }, function errorCallback(response) {
    	    		alert("Builds request failed");
    		  });
    	$scope.CILimit = 10;
    	$scope.expandCI = function(CILimit) {
    		$scope.CILimit += CILimit;
    	};
    	
    	$scope.TRLimit = 20;
    	$scope.expandTR = function(TRLimit) {
    		  $scope.TRLimit += TRLimit;
    		};

        
    });
