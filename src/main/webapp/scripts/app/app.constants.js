"use strict";

angular.module('vcm-tsp')

.constant('VERSION', '0.0.1-SNAPSHOT')

.factory('CodeShipAPI', function() {
  var api = {
              'codeship': {
                'projectId': '122329',
                'api': 'a4d37fb0dc8c0132649b025863fcc952',
                'url':'https://codeship.com/api/v1',
                'uri': 'https://codeship.com/api/v1/projects/122329.json?api_key=a4d37fb0dc8c0132649b025863fcc952'
              }
            };
    return {
        all: function() {
            return api;
        },
        uri: function(){
            return api.codeship.uri;
        }
    };
});

;
