'use strict';

angular.module('vcm-tsp')
    .factory('ShortestPathFactory', function ($resource, DateUtils, toastr) {
        return $resource('api/solver/shortestPath', {}, {
            'update': { 
            	method:'POST',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.date = DateUtils.convertDateTimeFromServer(data.date);
                    return data;
                },
            	interceptor: {
                	response: function(response){
            		    var time = response.config.responseTimestamp - response.config.requestTimestamp;
                    	toastr.success("Request took " + (time/1000) +" seconds");
                    	console.log(response);
                    	return response;
                	}
                }
            }
        })
    })
;