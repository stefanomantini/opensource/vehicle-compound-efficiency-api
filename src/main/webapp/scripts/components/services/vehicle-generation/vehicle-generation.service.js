'use strict';

angular.module('vcm-tsp')
    .factory('DataRandomSeed', function ($resource, DateUtils, toastr) {
        return $resource('api/generate-vehicles/random-seed/:seed/:nVehicles', {}, {
            'query': { 
            	method: 'GET', 
            	isArray: true,
            },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.date = DateUtils.convertDateTimeFromServer(data.date);
                    return data;
                }
            },
            'update': { method:'POST' }
        });
    }) 
    .factory('DataRandom', function ($resource, DateUtils, toastr) {
        return $resource('api/generate-vehicles/random/n/:nVehicles', {}, {
            'query': { 
            	method: 'GET', 
            	isArray: true,
            },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.date = DateUtils.convertDateTimeFromServer(data.date);
                    return data;
                }
            },
            'update': { method:'POST' }
        });
    })
    .factory('DataStatic', function ($resource, DateUtils, toastr) {
        return $resource('api//generate-vehicles/static-sample/:nVehicles', {}, {
            'query': { 
            	method: 'GET', 
            	isArray: true,
            },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.date = DateUtils.convertDateTimeFromServer(data.date);
                    return data;
                }
            },
            'update': { method:'POST' }
        });
    })
    .factory('DataMona', function ($resource, DateUtils, toastr) {
        return $resource('api/generate-vehicles/mona-sample/:nVehicles', {}, {
            'query': { 
            	method: 'GET', 
            	isArray: true,
            },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.date = DateUtils.convertDateTimeFromServer(data.date);
                    return data;
                }
            },
            'update': { method:'POST' }
        });
    })
    
;